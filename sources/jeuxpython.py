

########## Importer les modules necessaires ##############
from tkinter import *
from random import *
import time
from tkinter.font import Font
##########################################################
##########    Fonctions ##################################
########################################################## 
def affiche(L):
    """
    fonction qui affiche le plateau
    """
    i=0
    while i<10:
        j=0
        while j<len(L):
            if L[j][i]==1:
                Canevas.create_rectangle(100+90*i,50+90*j,190+90*i,140+90*j,fill='#63340b')
            if L[j][i]==0:
                Canevas.create_rectangle(100+90*i,50+90*j,190+90*i,140+90*j,fill='white')
            j=j+1
        i=i+1
def pion(P):
    """
    fonction permet d'afficher les pions actif sur le plateau
    """
    i=0
    while i<10:
        j=0
        while j<len(P):
            if P[j][i]==1:
                Canevas.create_oval(110+90*i,130+90*j,180+90*i,65+90*j,fill='#faf0e6')
            elif P[j][i]==2:
                Canevas.create_oval(110+90*i,130+90*j,180+90*i,65+90*j,fill='black')
            elif P[j][i]==3:
                Canevas.create_oval(110+90*i,130+90*j,180+90*i,65+90*j,fill='#faf0e6')
                Canevas.create_oval(130+90*i,110+90*j,160+90*i,85+90*j,fill='black')
            elif P[j][i]==4:
                Canevas.create_oval(110+90*i,130+90*j,180+90*i,65+90*j,fill='black')
                Canevas.create_oval(130+90*i,110+90*j,160+90*i,85+90*j,fill='#faf0e6')
            elif P[j][i]==0:
                i=i
            j=j+1
        i=i+1
def change_joueur():
    '''
    quand la fonction est appelé,cela doit changé de joueur
    ex:joueur1 = True joueur2 = False --> joueur1 = False joueur2 = True
    carré de couleur en fonction du joeur qui joue----red or green
    '''
    global joueur1
    global joueur2
    joueur1,joueur2=joueur2,joueur1
    if joueur1==True:
        Canevas.create_rectangle(1450,125,1650,225,fill='red')
        Canevas.create_rectangle(1450,785,1650,885,fill='green')
    else:
        Canevas.create_rectangle(1450,125,1650,225,fill='green')
        Canevas.create_rectangle(1450,785,1650,885,fill='red')
        
    


def Clic(event):
    '''
    trouver position dans la matrice
    voir si il y un pion de la couleur du tour_de_jeu
    Si pion cliqué de bonne couleur :
        - pion s'affiche en vers
        - on affiche les deplacementr possible
    sinon:
        rien
    '''
    global Vpion
    global joueur1
    global joueur2
    global pionini
    x=event.x
    y=event.y
    j=(x-100)//90
    i=(y-50)//90
    Vpion=P[i][j]
    pionini=[i,j]
    deplacement_Possible(i,j)
    Saut_Possible(i,j)
    if joueur1==True and Vpion==1 or Vpion==3:
        return Vpion,pionini
    elif joueur1==True and Vpion==2:
        return ()
    elif joueur2==True and Vpion==2 or Vpion==4:
        return Vpion,pionini
    elif joueur2==True and Vpion==1:
        return ()
    
        
    
def deplacement_Possible(i,j):
    '''
    retourne la liste des case possibles
    deplacemnt de tout les pions et des (dames)
    ex : deplacement_Posible(3,8) -> [[2,7],[4,7]] 
    '''
    global P
    global Vpion
    global deplacement
    o=1 
    
    if joueur1:
        if Vpion==1:
            if P[i-1][j-1]!=1 and P[i-1][j-1]!=2 and P[i-1][j-1]!=3 and P[i-1][j-1]!=4:
                deplacement.append([i-1,j-1])
            if P[i-1][j+1]!=1 and P[i-1][j+1]!=2 and P[i-1][j+1]!=3 and P[i-1][j+1]!=4:
                deplacement.append([i-1,j+1])
        if Vpion ==3:
                if i>j:
                    for k in range(j):
                        deplacement.append([i-o,j-o])
                        o+=1
                if i<j:
                    for k in range(i):
                        deplacement.append([i-o,j-o])
                        o+=1
                o=1
                if (10-i)>j:
                    for k in range(j):
                        deplacement.append([i-o,j+o])
                        o+=1
                if (10-i)<j:
                    for k in range(i):
                        deplacement.append([i-o,j+o])
                        o+=1
                o=1
                if i>(10-j):
                    for k in range(j):
                        deplacement.append([i+o,j-o])
                        o+=1
                if i<(10-j):
                    for k in range(i):
                        deplacement.append([i+o,j-o])
                        o+=1
                o=1
                if (10-i)>(10-j):
                    for k in range(j):
                        deplacement.append([i+o,j+o])
                        o+=1
                if (10-i)<(10-j):
                    for k in range(i):
                        deplacement.append([i+o,j+o])
                        o+=1
                o=1 
    if joueur2:
        if Vpion==2:
            if P[i+1][j-1]!=1 and P[i+1][j-1]!=2 and P[i+1][j-1]!=3 and P[i+1][j-1]!=4:
                deplacement.append([i+1,j-1])
            if P[i+1][j+1]!=1 and P[i+1][j+1]!=2 and P[i+1][j+1]!=3 and P[i+1][j+1]!=4:
                deplacement.append([i+1,j+1])
                
            if Vpion ==4:
                if P[i-o][j-o]!=1 and P[i-o][j-o]!=2 and P[i-o][j-o]!=3 and P[i-o][j-o]!=4:
                    while P[i-o][j-o]!=1 and P[i-o][j-o]!=2 and P[i-o][j-o]!=3 and P[i-o][j-o]!=4:
                        deplacement.append([i-o,j-o])
                        o+=1
                    o=1
                if P[i-o][j+o]!=1 and P[i-o][j+o]!=2 and P[i-o][j+o]!=3 and P[i-o][j+o]!=4:
                    while P[i-o][j+o]!=1 and P[i-o][j+o]!=2 and P[i-o][j+o]!=3 and P[i-o][j+o]!=4:
                        deplacement.append([i-o,j+o])
                        o+=1
                    o=1
                if P[i+o][j-o]!=1 and P[i+o][j-o]!=2 and P[i+o][j-o]!=3 and P[i+o][j-o]!=4:
                    while P[i+o][j-o]!=1 and P[i+o][j-o]!=2 and P[i+o][j-o]!=3 and P[i+o][j-o]!=4:
                        deplacement.append([i+o,j-o])
                        o+=1
                    o=1
                if P[i+o][j+o]!=1 and P[i+o][j+o]!=2 and P[i+o][j+o]!=3 and P[i+o][j+o]!=4:
                    while P[i+o][j+o]!=1 and P[i+o][j+o]!=2 and P[i+o][j+o]!=3 and P[i+o][j+o]!=4:
                        deplacement.append([i+o,j+o])
                        o+=1
                    o=1
            if Vpion ==4:
                if i>j:
                    for k in range(j):
                        deplacement.append([i-o,j-o])
                        o+=1
                if i<j:
                    for k in range(i):
                        deplacement.append([i-o,j-o])
                        o+=1
                o=1
                if (10-i)>j:
                    for k in range(j):
                        deplacement.append([i-o,j+o])
                        o+=1
                if (10-i)<j:
                    for k in range(i):
                        deplacement.append([i-o,j+o])
                        o+=1
                o=1
                if i>(10-j):
                    for k in range(j):
                        deplacement.append([i+o,j-o])
                        o+=1
                if i<(10-j):
                    for k in range(i):
                        deplacement.append([i+o,j-o])
                        o+=1
                o=1
                if (10-i)>(10-j):
                    for k in range(j):
                        deplacement.append([i+o,j+o])
                        o+=1
                if (10-i)<(10-j):
                    for k in range(i):
                        deplacement.append([i+o,j+o])
                        o+=1
                o=1
    return deplacement
            
        
            
    return deplacement
def Saut_Possible(i,j):
    '''
    retourne la liste des case possibles
    ex : deplacement_Posible(3,8) -> [[2,7],[4,7]]
    '''
    global P
    global Vpion
    global saut
    global Piomanger
    if joueur1:
        if Vpion==1:
            if P[i-1][j-1]==2 or P[i-1][j-1]==4 and P[i-2][j-2]!=1 and P[i-2][j-2]!=2 and P[i-2][j-2]!=3 and P[i-2][j-2]!=4:
                saut.append([i-2,j-2])
                Piomanger.append([i-1,j-1])
            if P[i-1][j+1]==2 or P[i-1][j+1]==4 and P[i-2][j+2]!=1 and P[i-2][j+2]!=2 and P[i-2][j+2]!=3 and P[i-2][j+2]!=4:
                saut.append([i-2,j+2])
                Piomanger.append([i-1,j+1])
            if P[i+1][j-1]==2 and P[i+2][j-2]!=1 and P[i+2][j-2]!=2 and P[i+2][j-2]!=3 and P[i+2][j-2]!=4:
                saut.append([i+2,j-2])
                Piomanger.append([i+1,j-1])
            if P[i+1][j+1]==2 and P[i+2][j+2]!=1 and P[i+2][j+2]!=2 and P[i+2][j+2]!=3 and P[i+2][j+2]!=4 :
                saut.append([i+2,j+2])
                Piomanger.append([i+1,j+1])
    
    if joueur2:
        if Vpion==2:
            if P[i-1][j-1]==1 and P[i-2][j-2]!=1 and P[i-2][j-2]!=2 and P[i-2][j-2]!=3 and P[i-2][j-2]!=4:
                saut.append([i-2,j-2])
                Piomanger.append([i-1,j-1])
            if P[i-1][j+1]==1 and P[i-2][j+2]!=1 and P[i-2][j+2]!=2 and P[i-2][j+2]!=3 and P[i-2][j+2]!=4:
                saut.append([i-2,j+2])
                Piomanger.append([i-1,j+1])
            if P[i+1][j-1]==1 and P[i+2][j-2]!=1 and P[i+2][j-2]!=2 and P[i+2][j-2]!=3 and P[i+2][j-2]!=4:
                saut.append([i+2,j-2])
                Piomanger.append([i+1,j-1])
            if P[i+1][j+1]==1 and P[i+2][j+2]!=1 and P[i+2][j+2]!=2 and P[i+2][j+2]!=3 and P[i+2][j+2]!=4 :
                saut.append([i+2,j+2])
                Piomanger.append([i+1,j+1])
    return deplacement,saut,Piomanger
def Depose(event):
    """
    verifie si possible 
    si possible change de place le pion et supprime le pion initial
    remet toute les variables utiliser a 0 pour rejouer un pion
    change le tour du joueur
    fonctionne pour les dames
    """
    global Vpion
    global pionini
    global deplacement
    global joueur1
    global joueur2
    global pionpose
    global P
    global saut
    pionpose=[]
    x=event.x
    y=event.y
    j=(x-100)//90
    i=(y-50)//90
    pionpose=[i,j] 
    S=0
    for t in saut:
        if pionpose==t:
            if P[i][j]==5:
                if Vpion==1:
                    k=pionini[0]
                    h=pionini[1]
                    P[k][h]=1
                    promotion(i,j)
                    Z=Piomanger[S][0]
                    W=Piomanger[S][1]
                    P[Z][W]=0
                if Vpion==2:
                    k=pionini[0]
                    h=pionini[1]
                    P[k][h]=2
                    promotion(i,j)
                    Z=Piomanger[S][0]
                    W=Piomanger[S][1]
                    P[Z][W]=0
            elif Vpion==1:
                k=pionini[0]
                h=pionini[1]
                P[k][h]=0
                P[i][j]=1
                promotion(i,j)
                Z=Piomanger[S][0]
                W=Piomanger[S][1]
                P[Z][W]=0
            elif Vpion==2:
                k=pionini[0]
                h=pionini[1]
                P[k][h]=0
                P[i][j]=2
                promotion(i,j)
                Z=Piomanger[S][0]
                W=Piomanger[S][1]
                P[Z][W]=0
        S+=1
    for t in deplacement:
        if pionpose==t:
            if P[i][j]==5:
                if Vpion==1:
                    k=pionini[0]
                    h=pionini[1]
                    P[k][h]=1
                    promotion(i,j)
                
                if Vpion==2:
                    k=pionini[0]
                    h=pionini[1]
                    P[k][h]=2
                    promotion(i,j)
            elif Vpion==1:
                k=pionini[0]
                h=pionini[1]
                P[k][h]=0
                P[i][j]=1
                promotion(i,j)
            elif Vpion==2:
                k=pionini[0]
                h=pionini[1]
                P[k][h]=0
                P[i][j]=2
                promotion(i,j)
            elif Vpion==3 or Vpion==4:
                k=pionini[0]
                h=pionini[1]
                P[k][h]=0
                P[i][j]=Vpion
    fin_de_jeu()
    deplacement=[]
    pionini=[]
    S=0
    affiche(L)   
    pion(P)
            
    
def promotion(i,j):
    '''
    quand un pion arrivera a i[0] ---> il devient une dame
    ses déplecement sont modifiés 
    sa maniere de manger aussi ( il s'etend plus loin )
    
    '''
    global Vpion
    global P    
    if Vpion==1:
        if i == 0:
            P[i][j] = 3
    if Vpion == 2:
        if i == 9:
            P[i][j] = 4
    affiche(L)
    pion(P)
    
    
    
    
def recommencer():
    """
    réinitinialise le plateau pour redemarrer une partie a deux
    """
    global P
    P=[[0,2,0,2,0,2,0,2,0,2],
       [2,0,2,0,2,0,2,0,2,0],
       [0,2,0,2,0,2,0,2,0,2],
       [2,0,2,0,2,0,2,0,2,0],
       [0,0,0,0,0,0,0,0,0,0],
       [0,0,0,0,0,0,0,0,0,0],
       [0,1,0,1,0,1,0,1,0,1],
       [1,0,1,0,1,0,1,0,1,0],
       [0,1,0,1,0,1,0,1,0,1],
       [1,0,1,0,1,0,1,0,1,0]]
    affiche(L)
    pion(P)
       
def fin_de_jeu ():
    global P
    global Mapolice
    a=P[0]
    b=P[1]
    c=P[2]
    d=P[3]
    e=P[4]
    f=P[5]
    g=P[6]
    h=P[7]
    i=P[8]
    j=P[9]
    nb_blanc=0
    nb_noir=0
    for n in a:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in b:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in c:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in d:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in e:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in f:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in g:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in h:
        if n==1:
            nb_blanc +=1
        if n==2:
            nb_noir +=1
    for n in i:
        if n==1:
            nb_blanc +=1
        if n==j:
            nb_noir +=1
    if nb_blanc==0:
        Canevas.create_text(1550,250,text="Joueur 2 a gagné",fill="brown",font=Mapolice)
    if nb_noir==0:
        Canevas.create_text(1550,850,text="Joueur 1 a gagné",fill="brown",font=Mapolice)
                
     
##########################################################
##########    Variables ##################################
##########################################################
L=[[0,1,0,1,0,1,0,1,0,1],
  [1,0,1,0,1,0,1,0,1,0],
  [0,1,0,1,0,1,0,1,0,1],
  [1,0,1,0,1,0,1,0,1,0],
  [0,1,0,1,0,1,0,1,0,1],
  [1,0,1,0,1,0,1,0,1,0],
  [0,1,0,1,0,1,0,1,0,1],
  [1,0,1,0,1,0,1,0,1,0],
  [0,1,0,1,0,1,0,1,0,1],
  [1,0,1,0,1,0,1,0,1,0]]

P=[[0,2,0,2,0,2,0,2,0,2],
   [2,0,2,0,2,0,2,0,2,0],
   [0,2,0,2,0,2,0,2,0,2],
   [2,0,2,0,2,0,2,0,2,0],
   [0,0,0,0,0,0,0,0,0,0],
   [0,0,0,0,0,0,0,0,0,0],
   [0,1,0,1,0,1,0,1,0,1],
   [1,0,1,0,1,0,1,0,1,0],
   [0,1,0,1,0,1,0,1,0,1],
   [1,0,1,0,1,0,1,0,1,0]]
Vpion=0
joueur1,joueur2=True,False # soit true joueur1 soit false pour le joueur 2 
pionpose=[]
deplacement=[]
saut=[]
Piomanger=[]
pionini=[]
#########################################################
########## Interface graphique ##########################
##########################################################
Mafenetre = Tk()
Mafenetre.title("Jeu de dame")
Canevas = Canvas(Mafenetre,width=1920,height=1080,bg ='black')
Canevas.pack()
Mapolice = Font(family='Liberation Serif', size=50) # création d'une police pour l'affichage du texte
Mapolice2 = Font(family='Liberation Serif', size=200)
Mapolice3 = Font(family='Liberation Serif', size=15)
###plateau
Canevas.create_rectangle(0,0,1150 ,1080,fill='#fdf1b8')
### joueur
Canevas.create_text(1550,50,text="joueur 2",fill="white",font=Mapolice)
Canevas.create_text(1550,930,text="joueur 1",fill="white",font=Mapolice)
Canevas.create_text(1550,350,text="finir le tour",fill="orange",font=Mapolice)
Canevas.create_rectangle(1450,125,1650 ,225,fill='red')
Canevas.create_rectangle(1450,785,1650 ,885,fill='green')

###message de fin
"""Canevas.create_text(960,540,text="You win!!!",fill="black",font=Mapolice2)"""

###########################################################
########### Receptionnaire d'évènement ####################
###########################################################
Canevas.bind("<Button-1>",Clic)
Canevas.bind("<Button-3>",Depose)
bouton=Button(Mafenetre, text="", command=change_joueur)
bouton.place(x=1450,y=400,width=200, height=200)
bouton=Button(Mafenetre, text="recommencer", command=recommencer)
bouton.place(x=1800,y=50)
##########################################################
############# Programme principal ########################
##########################################################
affiche(L)
pion(P)

###################### FIN ###############################  
Mafenetre.mainloop()
